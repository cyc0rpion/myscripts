#!/usr/bin/python

import socket
import subprocess
import sys
from protocolscan import protocol_scan

def identify_hostnames(ipaddr):
	print (socket.gethostbyaddr(ipaddr))

def identify_os(ipaddr):
	response = subprocess.Popen('ping -c 4 '+ipaddr,shell=True, stdout=subprocess.PIPE)
	output = response.communicate()[0]
	#print(output)

	#Test string for windows
	#output = "64 bytes from 192.168.0.6: icmp_seq=1 ttl=127 time=5.73 ms"

	if('ttl' in output):
		index = output.find('ttl')
		sub = output[index:index+7]
		sub = sub.strip()
		#print(sub)
		value = int(sub[4:])
		#print(value)

		if(value<=64 and value >=34):
			print(ipaddr+" is Linux")
		elif(value<=128 and value>=98):
			print(ipaddr+" is Windows")
		else:
			print("Sorry, could not identify OS")

	elif('Destination Host Unreachable' in output):
		print(ipaddr+" is Unreachable")
	else:
		print("ping failed for "+ipaddr)

ipaddr=sys.argv[1]
#identify_hostnames(ipaddr)
#identify_os(ipaddr)
print(protocol_scan("123"))