#!/usr/bin/python

import socket
import subprocess
import sys


def sshcbc_scan(pplist):
	
	#response = subprocess.Popen('nmap --script ssh2-enum-algos -iL iplist',shell=True, stdout=subprocess.PIPE)
	#output = response.communicate()[0]
	#print(output)

	iplist = ['192.168.0.1','192.168.0.2','192.168.0.3']

	output="""Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-05 01:49 IST
Nmap scan report for 192.168.0.1
Host is up (0.000083s latency).

PORT   STATE SERVICE
22/tcp open  ssh
| ssh2-enum-algos: 
|   kex_algorithms: (10)
|       curve25519-sha256
|       curve25519-sha256@libssh.org
|       ecdh-sha2-nistp256
|       ecdh-sha2-nistp384
|       ecdh-sha2-nistp521
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group16-sha512
|       diffie-hellman-group18-sha512
|       diffie-hellman-group14-sha256
|       diffie-hellman-group14-sha1
|   server_host_key_algorithms: (5)
|       rsa-sha2-512
|       rsa-sha2-256
|       ssh-rsa
|       ecdsa-sha2-nistp256
|       ssh-ed25519
|   encryption_algorithms: (6)
|       chacha20-poly1305@openssh.com
|       aes128-cbc
|       aes192-cbc
|       aes256-cbc
|       aes128-gcm@openssh.com
|       aes256-gcm@openssh.com
|   mac_algorithms: (10)
|       umac-64-etm@openssh.com
|       umac-128-etm@openssh.com
|       hmac-sha2-256-etm@openssh.com
|       hmac-sha2-512-etm@openssh.com
|       hmac-sha1-etm@openssh.com
|       umac-64@openssh.com
|       umac-128@openssh.com
|       hmac-sha2-256
|       hmac-sha2-512
|       hmac-sha1
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com

Nmap scan report for 192.168.0.2
Host is up (0.000083s latency).

PORT   STATE SERVICE
22/tcp open  ssh
| ssh2-enum-algos: 
|   kex_algorithms: (10)
|       curve25519-sha256
|       curve25519-sha256@libssh.org
|       ecdh-sha2-nistp256
|       ecdh-sha2-nistp384
|       ecdh-sha2-nistp521
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group16-sha512
|       diffie-hellman-group18-sha512
|       diffie-hellman-group14-sha256
|       diffie-hellman-group14-sha1
|   server_host_key_algorithms: (5)
|       rsa-sha2-512
|       rsa-sha2-256
|       ssh-rsa
|       ecdsa-sha2-nistp256
|       ssh-ed25519
|   encryption_algorithms: (6)
|       chacha20-poly1305@openssh.com
|       aes128-ctr
|       aes192-ctr
|       aes256-ctr
|       aes128-gcm@openssh.com
|       aes256-gcm@openssh.com
|   mac_algorithms: (10)
|       umac-64-etm@openssh.com
|       umac-128-etm@openssh.com
|       hmac-sha2-256-etm@openssh.com
|       hmac-sha2-512-etm@openssh.com
|       hmac-sha1-etm@openssh.com
|       umac-64@openssh.com
|       umac-128@openssh.com
|       hmac-sha2-256
|       hmac-sha2-512
|       hmac-sha1
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com

Nmap scan report for 192.168.0.3
Host is up (0.000083s latency).

PORT   STATE SERVICE
22/tcp open  ssh
| ssh2-enum-algos: 
|   kex_algorithms: (10)
|       curve25519-sha256
|       curve25519-sha256@libssh.org
|       ecdh-sha2-nistp256
|       ecdh-sha2-nistp384
|       ecdh-sha2-nistp521
|       diffie-hellman-group-exchange-sha256
|       diffie-hellman-group16-sha512
|       diffie-hellman-group18-sha512
|       diffie-hellman-group14-sha256
|       diffie-hellman-group14-sha1
|   server_host_key_algorithms: (5)
|       rsa-sha2-512
|       rsa-sha2-256
|       ssh-rsa
|       ecdsa-sha2-nistp256
|       ssh-ed25519
|   encryption_algorithms: (6)
|       chacha20-poly1305@openssh.com
|       aes128-cbc
|       aes192-cbc
|       aes256-cbc
|       aes128-gcm@openssh.com
|       aes256-gcm@openssh.com
|   mac_algorithms: (10)
|       umac-64-etm@openssh.com
|       umac-128-etm@openssh.com
|       hmac-sha2-256-etm@openssh.com
|       hmac-sha2-512-etm@openssh.com
|       hmac-sha1-etm@openssh.com
|       umac-64@openssh.com
|       umac-128@openssh.com
|       hmac-sha2-256
|       hmac-sha2-512
|       hmac-sha1
|   compression_algorithms: (2)
|       none
|_      zlib@openssh.com

Nmap done: 3 IP address (1 host up) scanned in 0.54 seconds"""

	#print(output)
	count = len(iplist)
	#print(count)

	outputList = [['foo' for x in range(3)] for y in range(3)] 

	#print(outputList)

	for i in range(count):
		outputList[i][0] = iplist[i]

	#print(outputList) 

	for i in range(count-1):
		start = output.find(iplist[i])
		end = output.find(iplist[i+1])

		#print(start)
		#print(end)


		outputList[i][1] = output[start:end]

	#for last IP
	start = output.find(iplist[-1])
	end = output.find("Nmap done")
	#print(start)
	#print(end)

	outputList[-1][1] = output[start:end]

	cbc = ""

	#print(outputList)

	for i in range(count):
		buf = outputList[i][1]
		start = buf.find("encryption_algorithms")
		end = buf.find("mac_algorithms")

		abuf = buf[start:end]

		#print(abuf)

		if("-cbc" in abuf):
			cbc += outputList[i][0]+"\n"

	#print(cbc)

	FinalCBC = ""
	FinalCBC += "\n[+] CBC Ciphers Mode Supported\n"
	FinalCBC += "-----------------"
	FinalCBC += "\nIP Address"
	FinalCBC += "\n-----------------\n"
	FinalCBC += cbc

	return(FinalCBC)

	